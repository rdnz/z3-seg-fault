module Lib where

import Z3.Monad
import Data.Maybe (fromMaybe)
import Control.Applicative (liftA2)

main :: IO ()
main = print =<< evalZ3 (do
  a <-
    (mkFreshConst
      "a"
      =<< bind2 mkArraySort mkIntSort mkIntSort
    )
  assert =<< mkTrue
  withModel $ \model ->
    traverse
      (\index ->
        fmap
          (fromMaybe (error "all integers are indexes")) $
        evalInt model =<< mkSelect a =<< mkInteger index
      )
      indexes
  )

indexes :: [Integer]
indexes = [0..2000]

bind2 :: Monad m => (a -> b -> m c) -> m a -> m b -> m c
bind2 f a b = uncurry f =<< liftA2 (,) a b
